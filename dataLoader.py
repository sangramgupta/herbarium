from __future__ import print_function, division

import json
import os
import warnings

import torch
from PIL import Image
from skimage import io
from torch.utils.data import Dataset
import cv2
import numpy as np
warnings.filterwarnings("ignore")


def showImageWithMarker(folderPath):
    annotationFile = folderPath + "metadata.json"
    with open(annotationFile, encoding="utf8", errors='ignore') as json_file:
        data = json.load(json_file)
        imageInfo = {}
        annotationImage = {}
        regions = {}
        categories = {}
        for category in data['categories']:
            categories[category['id']] = {'family': category['family'], 'genus': category['genus'],
                                          'name': category['name']}
        for region in data['regions']:
            regions[region['id']] = region['name']
        for image in data['images']:
            imageInfo[image['id']] = image['file_name']
        for annotation in data['annotations']:
            annotationImage[annotation['image_id']] = {'category_id': annotation['category_id'],
                                                       'region_id': annotation['region_id']}
        for id, fileName in imageInfo.items():
            imageLocation = os.path.join(folderPath, fileName)
            plantImage = io.imread(imageLocation)
            img = Image.fromarray(plantImage, 'RGB')
            img.show()

            category_id = annotationImage[id]['category_id']
            family = categories[category_id]['family']
            genus = categories[category_id]['genus']
            name = categories[category_id]['name']
            regionName = regions[annotationImage[id]['region_id']]

# showImageWithMarker("/home/sangram/Datasets/nybg2020/train/")

class HerbariumDataset(Dataset):

    def __init__(self, file_name, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """

        self.image = []
        annotationFile = root_dir + file_name
        with open(annotationFile, encoding="utf8", errors='ignore') as json_file:
            data = json.load(json_file)
            imageInfo = {}
            annotationImage = {}
            regions = {}
            categories = {}
            self.numClasses = len(data['categories'])
            for category in data['categories']:
                categories[category['id']] = {'family': category['family'], 'genus': category['genus'],
                                              'name': category['name']}
            for region in data['regions']:
                regions[region['id']] = region['name']
            for image in data['images']:
                imageInfo[image['id']] = image['file_name']
            for annotation in data['annotations']:
                annotationImage[annotation['image_id']] = {'category_id': annotation['category_id'],
                                                           'region_id': annotation['region_id']}
            for id, fileName in imageInfo.items():
                imageLocation = os.path.join(root_dir, fileName)

                category_id = annotationImage[id]['category_id']
                family = categories[category_id]['family']
                genus = categories[category_id]['genus']
                name = categories[category_id]['name']
                regionName = regions[annotationImage[id]['region_id']]

                self.image.append(
                    {'class': category_id, 'family': family, 'genus': genus, 'name': name, 'regionName': regionName, 'imageLocation': imageLocation})

        self.transform = transform

    def __len__(self):
        return len(self.image)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        originalImage = io.imread(self.image[idx]['imageLocation'])
        resizedImage = np.transpose(cv2.resize(originalImage, dsize=(256, 256), interpolation=cv2.INTER_CUBIC),(2,0,1))
        # resizedImage = cv2.resize(originalImage, dsize=(100, 100), interpolation=cv2.INTER_CUBIC)
        annotations = np.zeros(( self.numClasses))
        # annotations[0,0] = 0
        # annotations[0,1] = 0
        # annotations[0,2] = 31
        # annotations[0,3] = 31
        annotations[self.image[idx]['class']] = 1
        # sample = {'image': torch.from_numpy(resizedImage), 'annotation': annotations}
        sample = {'image': resizedImage, 'annotation': annotations}

        if self.transform:
            sample = self.transform(sample)

        return sample


# dataset = HerbariumDataset('metadata.json', '/home/sangram/Datasets/nybg2020/train/')


