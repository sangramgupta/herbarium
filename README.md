# Herbarium

Problem Statement:

Visual Dataset and Object Detection on multiple ids. 
https://www.kaggle.com/c/herbarium-2020-fgvc7/data

**Approach 1:**

Use object detection models. Having previously worked with Retinanet, goto approach was that.
Pytorch Model Inspired from https://github.com/yhenon/pytorch-retinanet
Created a custom dataloader so that we can feed optimised data into the model.

Observed that too much memory is being taken and only ResNet18 is being able to load, that sounds a very dismal performance.
Compared the reasons why it might be happening. Optimised tensors to have auto-grad or not. Checked the usual batch size and image size input.
Fed 32*32 image so can't possible go below than that.

Regression model is disabled everywhere and so that does not cause any issue. 

Just realised the classes in this case are 40k compared to other datasets that I have workd with which only contained at most 1k.

Do some simple checks and it is confirmed that this is the reason why data model explodes, probably because of the last linear layer.
And yes the anchor boxes per class is the most obvious reason. 

Will investigate newer architectures.


**Approach 2:** 

Just use simple 1 stage architectures, with classification models and see whether they work or not.
