import argparse
import collections
from builtins import type

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

import SeNetVariants
import model
from dataLoader import HerbariumDataset

assert torch.__version__.split('.')[0] == '1'

print('CUDA available: {}'.format(torch.cuda.is_available()))


def main(args=None):
    parser = argparse.ArgumentParser(description='Simple training script for training a RetinaNet network.')

    parser.add_argument('--architecture', help='RetinaNet, SeResNext', type=str, default="SeResNext")
    parser.add_argument('--depth', help='Resnet depth, must be one of 18, 34, 50, 101, 152', type=int, default=18)
    parser.add_argument('--epochs', help='Number of epochs', type=int, default=100)

    parser = parser.parse_args(args)

    dataset_train = HerbariumDataset('metadata.json', '/home/sangram/Datasets/nybg2020/train/')

    dataloader_train = DataLoader(dataset_train, batch_size=4, shuffle=True, num_workers=0)
    # dataloader_validation = dataloader_train

    # Create the model
    if parser.architecture == "SeResNext":
        modelArchitecture = SeNetVariants.se_resnext50_32x4d(num_classes=32094, pretrained=False)
    if parser.architecture == "RetinaNet":
        if parser.depth == 18:
            modelArchitecture = model.resnet18(num_classes=32094, pretrained=True)
        elif parser.depth == 34:
            modelArchitecture = model.resnet34(num_classes=32094*4, pretrained=True)
        elif parser.depth == 50:
            modelArchitecture = model.resnet50(num_classes=32094, pretrained=True)
        elif parser.depth == 101:
            modelArchitecture = model.resnet101(num_classes=32094*4, pretrained=True)
        elif parser.depth == 152:
            modelArchitecture = model.resnet152(num_classes=32094*4, pretrained=True)
        else:
            raise ValueError('Unsupported model depth, must be one of 18, 34, 50, 101, 152')

    use_gpu = True

    if use_gpu:
        if torch.cuda.is_available():
            modelArchitecture = modelArchitecture.cuda()

    if torch.cuda.is_available():
        modelArchitecture = torch.nn.DataParallel(modelArchitecture).cuda()
    else:
        modelArchitecture = torch.nn.DataParallel(modelArchitecture)

    modelArchitecture.training = True

    optimizer = optim.Adam(modelArchitecture.parameters(), lr=1e-5)

    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=3, verbose=True)

    loss_hist = collections.deque(maxlen=500)

    modelArchitecture.train()
    criterion = nn.CrossEntropyLoss().cuda()
    # modelArchitecture.module.freeze_bn()

    print('Num training images: {}'.format(len(dataset_train)))

    for epoch_num in range(parser.epochs):

        modelArchitecture.train()
        # modelArchitecture.module.freeze_bn()

        epoch_loss = []

        for iter_num, data in enumerate(dataloader_train):

            optimizer.zero_grad()

            if torch.cuda.is_available():
                classification_output = modelArchitecture(data['image'].cuda().float())
                # classification_loss = modelArchitecture([data['image'].cuda().float(), data['annotation']])
            else:
                classification_output = modelArchitecture(data['image'].float())
                # classification_loss = modelArchitecture([data['image'].float(), data['annotation']])

            target = torch.argmax(data['annotation'], dim=1).long().cuda()
            classification_loss = criterion(classification_output,target)

            loss = classification_loss

            if bool(loss == 0):
                continue

            loss.backward()

            torch.nn.utils.clip_grad_norm_(modelArchitecture.parameters(), 0.1)

            optimizer.step()

            loss_hist.append(float(loss))

            epoch_loss.append(float(loss))

            print(
                'Epoch: {} | Iteration: {} | Classification loss: {:1.5f} | Running loss: {:1.5f}'.format(
                    epoch_num, iter_num, float(classification_loss), np.mean(loss_hist)))

                # del classification_loss
                # del regression_loss


        scheduler.step(np.mean(epoch_loss))

        torch.save(modelArchitecture.module, '{}_retinanet_{}.pt'.format(parser.dataset, epoch_num))

    modelArchitecture.eval()

    torch.save(modelArchitecture, 'model_final.pt')


if __name__ == '__main__':
    main()